package com.example.dictionary.util;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

@Component
public class ResourceReader {
    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    public String asString(Resource resource) {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String readFileToString(String path) {
        Resource resource = resourceLoader.getResource(path);
        return asString(resource);
    }

    public InputStreamReader getResourceInputStreamReader(String path) throws IOException {
        Resource resource = resourceLoader.getResource(path);
        return new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8);
    }
}
