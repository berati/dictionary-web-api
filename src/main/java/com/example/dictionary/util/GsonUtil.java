package com.example.dictionary.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

@Component
public class GsonUtil {
    private static Gson prettyGson;

    public static Gson getPrettyJson() {
        if (prettyGson == null) {
            prettyGson = new GsonBuilder().setPrettyPrinting().create();
        }
        return prettyGson;
    }

}
