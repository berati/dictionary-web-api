package com.example.dictionary.resources.controller;

import com.example.dictionary.data.model.Word;
import com.example.dictionary.data.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/words")
public class WordController {
    @Autowired
    WordRepository wordRepository;

    // @RequestMapping(value = "/", method = RequestMethod.GET)
    @GetMapping("/")
    List<Word> getAll() {
        return wordRepository.findAll();
    }

    // @GetMapping("/{value}")
    // Word createSomeWord(@PathVariable("value") String wordValue)
}
