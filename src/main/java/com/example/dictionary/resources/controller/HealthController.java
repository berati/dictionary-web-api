package com.example.dictionary.resources.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/health")
public class HealthController {
    // @RequestMapping(value = "/", method = RequestMethod.GET)
    @GetMapping("/")
    String health() {
        return "OK";
    }
}
