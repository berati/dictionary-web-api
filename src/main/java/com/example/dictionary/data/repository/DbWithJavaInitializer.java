package com.example.dictionary.data.repository;


import com.example.dictionary.data.model.LangCode;
import com.example.dictionary.data.model.Language;
import com.example.dictionary.data.model.Word;
import com.example.dictionary.data.model.WordType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@Profile("dbSeedWithJava")
@Component
@Slf4j // for logging
public class DbWithJavaInitializer implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private WordRepository wordRepository;

    private Language lang1, lang2, lang3;
    private Word tr_word1, tr_word2, tr_word3;
    private Word en_word1, en_word2, en_word3;
    private Word de_word1, de_word2, de_word3;

    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("Database seed: start");
        seedLanguages();
        seedTrWords();
        seedEnWords();
        seedDeWords();
        seedTranslations();
        log.info("Database seed: end");
    }

    @Transactional
    private void seedLanguages() {
        lang1 = new Language(LangCode.TR);
        lang2 = new Language(LangCode.DE);
        lang3 = new Language(LangCode.EN);
        languageRepository.save(lang1);
        languageRepository.save(lang2);
        languageRepository.save(lang3);
    }

    @Transactional
    private void seedTrWords() {
        tr_word1 = new Word(lang1, WordType.NOUN, "takim arkadasi");
        tr_word2 = new Word(lang1, WordType.ADJECTIVE, "nazik");
        tr_word3 = new Word(lang1, WordType.VERB, "okumak");
        List<Word> tr_wordList = Arrays.asList(
                tr_word1,
                tr_word2,
                tr_word3
        );
        wordRepository.saveAll(tr_wordList);
    }

    @Transactional
    private void seedEnWords() {
        en_word1 = new Word(lang2, WordType.NOUN, "teammate");
        en_word2 = new Word(lang2, WordType.ADJECTIVE, "gentle");
        en_word2 = new Word(lang2, WordType.ADJECTIVE, "read");
        List<Word> en_wordList = Arrays.asList(
                en_word1,
                en_word2,
                en_word3
        );
        wordRepository.saveAll(en_wordList);
    }

    @Transactional
    private void seedDeWords() {
        de_word1 = new Word(lang3, WordType.NOUN, "Teamkollege");
        de_word2 = new Word(lang3, WordType.ADJECTIVE, "zart");
        de_word3 = new Word(lang3, WordType.ADJECTIVE, "lesen");
        List<Word> de_wordList = Arrays.asList(
                de_word1,
                de_word2,
                de_word3
        );
        wordRepository.saveAll(de_wordList);
    }

    @Transactional
    private void seedTranslations() {
        tr_word1.addTranslation(en_word1);
        de_word1.addTranslation(en_word1);
        wordRepository.saveAll(Arrays.asList(tr_word1, de_word1));
    }
}
