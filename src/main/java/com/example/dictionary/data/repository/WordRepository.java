package com.example.dictionary.data.repository;

import com.example.dictionary.data.model.Word;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.OneToMany;
import java.util.List;

@Repository
public interface WordRepository extends CrudRepository<Word, String> {
    @Override
    List<Word> findAll();

    Word findByValue(String value);


}
