package com.example.dictionary.data.repository;


import com.example.dictionary.data.json.WordExampleJsonData;
import com.example.dictionary.data.json.WordJsonData;
import com.example.dictionary.data.model.*;
import com.example.dictionary.util.GsonUtil;
import com.example.dictionary.util.ResourceReader;
import com.google.gson.reflect.TypeToken;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Profile("dbSeedWithJson")
@Component
@Slf4j // for logging
public class ParseJsonAndSeedDbInitializer implements ApplicationListener<ApplicationReadyEvent> {
    // Logger logger = LoggerFactory.getLogger(DbInitializer.class);
    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private ResourceReader resourceReader;

    @Autowired
    private WordExampleRepository wordExampleRepository;

    @SneakyThrows
    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("Database seed with JSON: start");
        seedLanguages();
        seedWords();
        seedWordExamples();
        log.info("Database seed with JSON: end");
    }

    @Transactional
    public void seedLanguages() throws IOException {
        InputStreamReader langStreamReader = resourceReader.getResourceInputStreamReader("classpath:data/languages.json");
        Type gsonLangListType = new TypeToken<ArrayList<Language>>() {}.getType();
        List<Language> languageList = GsonUtil.getPrettyJson().fromJson(langStreamReader, gsonLangListType);
        languageRepository.saveAll(languageList);
    }

    @Transactional
    public void seedWords() throws IOException {
        InputStreamReader wordStreamReader = resourceReader.getResourceInputStreamReader("classpath:data/word.json");
        Type gsonWordJsonDataListType = new TypeToken<ArrayList<WordJsonData>>() {}.getType();
        List<WordJsonData> wordJsonDataList = GsonUtil.getPrettyJson().fromJson(wordStreamReader, gsonWordJsonDataListType);

        List<Word> words = new ArrayList<>();
        for(WordJsonData jsonData : wordJsonDataList) {
            final Language lang = languageRepository.findById(jsonData.getLangCode()).orElse(null);
            final Word word = new Word(lang, jsonData.getType(), jsonData.getValue());

            words.add(word);
        }

        wordRepository.saveAll(words);
    }


    @Transactional
    public void seedWordExamples() throws IOException {
        InputStreamReader wordExampleStreamReader = resourceReader.getResourceInputStreamReader("classpath:data/word-examples.json");
        Type gsonWordExampleJsonDataListType = new TypeToken<ArrayList<WordExampleJsonData>>() {}.getType();
        List<WordExampleJsonData> wordExampleJsonDataList = GsonUtil.getPrettyJson().fromJson(wordExampleStreamReader, gsonWordExampleJsonDataListType);

        List<WordExample> wordExamples = new ArrayList<>();

        for(WordExampleJsonData exampleJsonData : wordExampleJsonDataList){
             Word word = wordRepository.findByValue(exampleJsonData.getWordVal());
             WordExample wordExample = new WordExample(exampleJsonData.getExample(),word);
             wordExamples.add(wordExample);
        }

        wordExampleRepository.saveAll(wordExamples);

    }
}
