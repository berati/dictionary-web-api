package com.example.dictionary.data.repository;

import com.example.dictionary.data.model.WordExample;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordExampleRepository extends CrudRepository <WordExample, String> {

}
