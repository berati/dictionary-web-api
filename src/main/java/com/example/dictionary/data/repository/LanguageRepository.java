package com.example.dictionary.data.repository;

import com.example.dictionary.data.model.LangCode;
import com.example.dictionary.data.model.Language;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LanguageRepository extends CrudRepository<Language, LangCode> {
}
