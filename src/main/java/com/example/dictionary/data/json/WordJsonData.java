package com.example.dictionary.data.json;

import com.example.dictionary.data.model.LangCode;
import com.example.dictionary.data.model.WordType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WordJsonData {
    private LangCode langCode;
    private WordType type;
    private String value;
}
