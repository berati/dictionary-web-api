package com.example.dictionary.data.json;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WordExampleJsonData {
    private String wordVal;
    private String example;
}
