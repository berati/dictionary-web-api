package com.example.dictionary.data.model;

import com.example.dictionary.data.repository.WordExampleRepository;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter

@Entity
@Table(name = "brt_word_example")
public class WordExample {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    @Column(nullable = false)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    private Word word;


    public WordExample(String value, Word word) {
        this.value = value;
        this.word = word;
    }


}