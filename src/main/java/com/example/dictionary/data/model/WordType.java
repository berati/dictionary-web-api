package com.example.dictionary.data.model;

public enum WordType {
    NOUN,
    ADVERB,
    ADJECTIVE,
    VERB,
    PREPOSITION,
    CONJUCTION
}
