package com.example.dictionary.data.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "brt_word")
public class Word {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String value;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private WordType type;

    @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "my_language_idasdasdas", referencedColumnName = "code", nullable = false)
    @JoinColumn(nullable = false)
    private Language language;

    @ManyToMany(fetch = FetchType.LAZY)
    @Getter(value = AccessLevel.NONE)
    private Set<Word> translations;

    @OneToMany(mappedBy = "word", fetch = FetchType.LAZY)
    @Getter(value = AccessLevel.NONE)
    private Set<WordExample> examples;

    public Word() {
    }

    public Word(Language lang, WordType type, String value) {
        this.language = lang;
        this.type = type;
        this.value = value;
    }

    public Set<Word> getTranslations() {
        if(translations == null) {
            translations = new HashSet<>();
        }
        return translations;
    }

    public Set<WordExample> getExamples() {
        if(examples == null) {
            examples = new HashSet<>();
        }
        return examples;
    }

    public void addTranslation(Word... words) {
        for(Word w : words) {
            getTranslations().add(w);
        }
    }

    public void addExample(WordExample... wordExamples) {
        for(WordExample wordExample : wordExamples) {
            getExamples().add(wordExample);
        }
    }

}