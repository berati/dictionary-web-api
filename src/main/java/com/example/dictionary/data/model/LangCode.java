package com.example.dictionary.data.model;

public enum LangCode {
    EN,
    TR,
    DE,
    ES,
}
