package com.example.dictionary.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Setter
@Getter
// @NoArgsConstructor -> bos constructor
// @AllArgsConstructor -> constructor with all arguments
@Entity
@Table(name = "brt_language")
public class Language {
    @Id
    @Enumerated(EnumType.STRING)
    private LangCode code;

    @OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
    private Set<Word> words;

    public Language() {}

    public Language(LangCode code) {
        this.code = code;
    }

    public Language(LangCode code, Set<Word> words) {
        this.code = code;
        this.words = words;
    }
}