# Dictionary Web API

This is the RESTful Web API of the dictionary project.

## Docker services

### Running database
```
docker-compose --project-name dictionary-web-api up -d
```

###  Stopping database
```
docker-compose --project-name dictionary-web-api stop
```

###  Removing database (danger)
```
docker-compose --project-name dictionary-web-api down --volumes --remove-orphans
```
